## Used Frameworks
Ionic / Angular

Ionic CLI 5.4.12

## Prerequisites
npm install -g @ionic/cli

npm install -g cordova

## Start

npm install

npm run start:api

npm run start:wb

## Explanation
To get the full explanation check out this page https://love2dev.com/blog/what-is-a-service-worker/

<img src="https://love2dev.com/img/service-worker-diagram-b7984584e67f70d488c1a078f92d846b.png"
     alt="Markdown Monster icon"/>

<img src="https://love2dev.com/img/simple-service-worker-cache-86f36fd7b5826a1bdb607cf485c57d93.png"
     alt="Markdown Monster icon" />
