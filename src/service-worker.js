importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.5.3/workbox-sw.js')

//The new installed service worker replaces the old service worker immediately
self.skipWaiting();

workbox.setConfig({
  debug: false,
})

//Test workbox
if (workbox) {
  console.log('Workbox is loaded new');
} else {
  console.log('Workbox did not loaded');
}


const bgSyncPlugin = new workbox.backgroundSync.BackgroundSyncPlugin('queue', {
  maxRetentionTime: 24 * 60 // Retry for max of 24 Hours
});

const queuePlugin = new workbox.backgroundSync.Queue('syncQueue')
const precacheManifest = [injectionPoint];

workbox.precaching.precacheAndRoute(precacheManifest.map(route => { // checking for valid entries
  if (route.url && route.revision) {
    return route
  }
}).filter(route => route));


new workbox.precaching.addPlugins('app-shell');

// Event listeners

self.addEventListener('fetch', (event) => {
  const bgSyncLogic = async () => {
    try {
      const res = await fetch(event.request.clone());
      return res;
    } catch(err) {
      console.log('Adding Request to Queue');
      if ((await queuePlugin.getAll()).length >= 1) {
        console.log('Shifting Queue');
        queuePlugin.shiftRequest()
      }
      await queuePlugin.pushRequest({request: event.request});
      return err;
    }
  }

  if ((event.request.url).includes('http://localhost:3000/test')) {
    if (event.request.method !== 'GET') {
      event.respondWith(bgSyncLogic());
    }
  }
});

const broadcast = new BroadcastChannel('test123');

broadcast.onmessage = (msg) => {
  if (msg.data.type === 'dosync') {
    console.log('Im syncing now');
    queuePlugin.replayRequests().then(() => broadcast.postMessage({
      type: 'doSync',
      payload: 'success'
    }));
  }
}