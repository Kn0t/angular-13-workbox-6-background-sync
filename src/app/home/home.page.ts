import { BroadcastService } from './../services/broadcast.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, interval } from 'rxjs';
import { take, takeUntil, filter, mergeMap, concatMap, retryWhen, delay } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name: string;
  isOnline = true;
  didChanged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(
    private http: HttpClient,
    private broadcast: BroadcastService
  ) {}

  buttonPressed(name: string) {
    this.http.patch('http://localhost:3000/test/1', {name}).subscribe(
      (res: any) => {
        this.name = res.name;
      },
      err => {
        console.error('got err', err);
      }
    );
  }

  ngOnInit(): void {
    this.getLatest();

    window.addEventListener('online', () => {
      this.broadcast.publish({
        type: 'dosync',
        payload: 'start'
      });

      this.isOnline = true;
    });

    this.broadcast.onMessage.subscribe(res => {
      if (res.type === 'doSync') {
        if (res.payload === 'success') {
          console.log('I did sync');
          setTimeout(() => this.getLatest(), 1000); // backend is to slow
        }
      }
    });

    window.addEventListener('offline', () => {
      this.isOnline = false;
    });
  }

  getLatest() {
    this.didChanged.next(false);

    interval(50).pipe(
      filter(() => !this.didChanged.value),
      take(10),
      concatMap(() => this.http.get('http://localhost:3000/test/1'))
    ).subscribe(
      (res: any) => {
        if (this.name === res.name) {
          return;
        }

        this.name = res.name;
        this.didChanged.next(true);
      }
    );
  }

}
