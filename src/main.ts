import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Workbox } from 'workbox-window';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .then(() => {
    if ('serviceWorker' in navigator) {
      const wb = new Workbox('service-worker.js');
      wb.register();

      wb.addEventListener('installed', (event) => {
        console.log('wb, installed');
        if (event.isUpdate) {
          console.log('isUpdate');
        } else {
          console.log('App is offline ready');
        }
      });
    }
  })
  .catch(err => console.log(err));
