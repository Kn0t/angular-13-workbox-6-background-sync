const { injectManifest } = require('workbox-build');

let workboxConfig = {
  globDirectory: 'www/',
  globPatterns: [
    '**/*.{txt,png,ico,html,js,json,css}'
  ],
  swSrc: 'src/service-worker.js',
  swDest: 'www/service-worker.js',
  injectionPoint: 'injectionPoint'
}

injectManifest(workboxConfig)
  .then(({count, size}) => {
    console.log(`Generated ${workboxConfig.swDest}, wich will precache ${count} files, ${size} bytes.`);
  })
  .catch(err => {
    console.error('failed injection');
    console.error(err);
  })